# -*- coding=utf-8 -*-
#!/usr/bin/env python

import os
import json
from bigdata_util.util import get_absolute_path
import pydash
from pyhocon import ConfigFactory
from pyhocon.converter import HOCONConverter


common_config_path = get_absolute_path(__file__, './common_config.json')
local_config_path = get_absolute_path(__file__, './local_config.json')

# 读取通用配置
config_dict = json.load(open(common_config_path))

# 本地如果存在配置则合并进来
if os.path.isfile(local_config_path):
    local_config = json.load(open(local_config_path))
    pydash.objects.assign_with(config_dict, local_config)
    pass

print(config_dict)
config = ConfigFactory.from_dict(config_dict)

def update_config(c, j):
    config_dict = json.loads(HOCONConverter.to_json(c))
    pydash.objects.assign_with(config_dict, j)
    global config
    config = ConfigFactory.from_dict(config_dict)
