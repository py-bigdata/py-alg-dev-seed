FROM --platform=linux/amd64 mfwbsg51.mirror.aliyuncs.com/library/python:3.8

# RUN sed -i s@/deb.debian.org/@/mirrors.aliyun.com/@g /etc/apt/sources.list && apt-get clean && apt-get update && apt-get install -y net-tools vim procps openjdk-11-jdk unzip pip libgdal-dev
RUN sed -i s@/deb.debian.org/@/mirrors.163.com/@g /etc/apt/sources.list && sed -i s@/security.debian.org/@/mirrors.163.com/@g /etc/apt/sources.list
RUN apt-get clean && apt-get update && apt-get install -y net-tools vim procps unzip pip libgdal-dev iputils-ping telnet openjdk-11-jdk
# RUN yum install -y make pkgconfig unzip which rsync net-tools vim telnet libcurl-devel openssl openssl-devel zlib zlib-devel sqlite* graphviz graphviz-devel tkinter tcl-devel tk-devel psmisc cronolog-1.6.2 bc

#add admin user
RUN groupadd admin
RUN useradd admin -g admin
RUN mkdir -p /home/admin
RUN chown admin:admin /home/admin
RUN rm -rf /etc/localtime

# 设置时区
ENV TZ=Asia/Shanghai \
    DEBIAN_FRONTEND=noninteractive
RUN ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime \
    && echo ${TZ} > /etc/timezone \
    && dpkg-reconfigure --frontend noninteractive tzdata \
    && rm -rf /var/lib/apt/lists/*

RUN export LANG="en_US.UTF-8"
RUN echo 'export LANG="en_US.UTF-8"' >> ~/.bashrc
RUN echo 'export LANG="en_US.UTF-8"' >> /home/admin/.bashrc

RUN export CPLUS_INCLUDE_PATH=/usr/include/gdal
RUN echo 'export CPLUS_INCLUDE_PATH=/usr/include/gdal' >> ~/.bashrc
RUN echo 'export CPLUS_INCLUDE_PATH=/usr/include/gdal' >> /home/admin/.bashrc

RUN export C_INCLUDE_PATH=/usr/include/gdal
RUN echo 'export C_INCLUDE_PATH=/usr/include/gdal' >> ~/.bashrc
RUN echo 'export C_INCLUDE_PATH=/usr/include/gdal' >> /home/admin/.bashrc
RUN echo 'export PATH=${PATH}:/home/admin/.local/bin' >> /home/admin/.bashrc

# Install pip requirements
USER admin
COPY requirements.txt .
# RUN --mount=type=cache,target=/home/admin/.cache pip install -r requirements.txt
RUN pip install -r requirements.txt
USER root

RUN export SHELL=/bin/bash
SHELL ["/bin/bash", "-c"]

COPY bin/ /
RUN chmod +x /start.sh && bash /init.sh
RUN chmod +x /docker-entrypoint.sh
RUN echo "alias ll='ls -l --color=auto'" >> /home/admin/.bashrc
USER admin

ENTRYPOINT ["/docker-entrypoint.sh"]
# CMD ["bash", "-c", "/start.sh"]
