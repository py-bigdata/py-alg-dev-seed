#!/usr/bin/env python
# -*- coding=utf-8 -*-

import sys
import json
from config import config, update_config


if __name__ == '__main__':
    print(sys.argv)

    print(' params: ==> \n')
    # argv用空格识别参数，dataq输出的参数带空格时会被识别为多个参数
    argv1 = ' '.join(sys.argv[1:])

    if argv1.startswith('{') and argv1.endswith('}'):
        params = json.loads(argv1)
    else:
        params = {
            "app_env": "develop"
        }
    update_config(config, params)

    print(json.dumps(params, indent=4))
    print('\n params: <==')

    from src.main import Main
    Main().run()