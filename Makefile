dict_name=$(shell basename `pwd`)
work_path=$(shell echo `pwd`)

build:
	@docker image prune -f
	@export DOCKER_BUILDKIT=0 && export COMPOSE_DOCKER_CLI_BUILD=0 && docker build -t img-${dict_name} .

docker_debug:
	@docker run -it --rm -v ${work_path}:/home/admin/source --name debug-${dict_name} img-${dict_name} bash

pack: clean install_pip
	@zip -r ${dict_name}.zip *.py src lib -x "*.csv" -x "*.pyc" -x "*conf.ini" -x "config.conf"

install_pip:
	@pip install -t lib -r requirements.txt

clean_docker:
	docker image prune -f
	docker images | awk '{print $3}' | xargs docker rmi

clean:
	@rm -fr ${dict_name}.zip

save:
	docker save img-${dict_name} | gzip > ${dict_name}-latest.tar.gz

test:
	@echo "start testing..."
	PYTHONPATH=. python test/main.py
	@echo "test done."

.PHONY: build docker_debug pack install_pip clean test
