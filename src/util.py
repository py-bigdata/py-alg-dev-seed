# -*- coding=utf-8 -*-
#!/usr/bin/env python

from bigdata_util.connector import PostgreConnector
from config import config


class Util:

    @staticmethod
    def get_pg_con():
        if 'pg_uri' not in config:
            return None

        return PostgreConnector(config.get('pg_uri'))

