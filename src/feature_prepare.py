# -*- coding=utf-8 -*-
#!/usr/bin/env python


class FeaturePrepare:

    @staticmethod
    def get_batch_data():
        for idx in range(1000):
            yield [idx]

    @staticmethod
    def foo():
        return 'bar';
