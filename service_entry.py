#!/usr/bin/env python
# -*- coding=utf-8 -*-

from flask import Flask

app = Flask('app')

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

@app.route('/hello')
def index():
    return {
        'hello': 'world!'
    }

if __name__ == "__main__":
    app.run(
        debug=True,
        host='0.0.0.0',
        port=5001
    )