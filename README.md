## py-standalone / py-service 种子项目

### 初始化项目

1. `pip install algboost --upgrade`
2. `algboost-project init <project_name>`
3. `cd <project_name>`
4. 设置git源地址: `git remote set-url origin <gitlab新建的地址>`

### 镜像模式 - 开发

1. 使用vscode打开，安装 "Remote - Containers"插件
2. 点击左下角 “><” 在镜像中调试，选择“Reopen in Container”
3. 首次镜像构建比较久，构建完成后可在容器中调试

py-service入口: `python service_entry.py`
py-standalone入口: `python standalone_entry.py`

#### 容器中使用matplotlib

1. Install XQuartz: https://www.xquartz.org/
2. Launch XQuartz. Under the XQuartz menu, select Preferences
3. Go to the security tab and ensure "Allow connections from network clients" is checked.
4. Start XQuartz and run `xhost +localhost` in terminal.

第4步每次都需要操作

### 单元测试

1. 如果vscode左侧的`Testing`标签页没出来，需要在`Extensions`标签页中重装`Python`扩展，重装重启后可在`Testing`中进行单元测试

### 现场部署

1. 【本地执行】本地构建镜像，如果采用dev container的方式开发，本地已经有镜像，可以跳过这一步；如果本地没有镜像，可在项目根目录执行命令(注意替换project_name)
    - `docker build -t reg.docker.aliyun.com/py-bigdata/${project_name}:master .`
2. 【本地执行】执行docker save命令将镜像保存到本地:
    - `docker save reg.docker.aliyun.com/py-bigdata/${project_name}:master |gzip > img.tar.gz`
3. 【本地执行】想办法将镜像传到部署目标机器，以scp为例，注意替换target_ip为目标机器ip
    - `scp img.tar.gz root@${target_ip}:`
4. 【部署机器】执行docker load加载镜像
    - `gunzip -c img.tar.gz | docker load`
5. 【部署机器】执行`docker images`检查`reg.docker.aliyun.com/py-bigdata/${project_name}:master` 已存在
6. 【本地执行】修改`docker-compose.yml`中的 command命令，两种模式
    - 常驻api服务: `python app.py`
    - 其他: `tail -f /dev/null`
7. 【本地执行】将本地工程目录复制到远程机器上: 注意替换 folder_name 是项目文件夹名称，target_ip 是部署机器ip
    - `cd .. && tar cfz code.tar.gz ${folder_name}`
    - `scp code.tar.gz root@${target_ip}`
8. 【部署机器】解压并启动服务
    - 解压代码: `tar xfz code.tar.gz && cd ${folder_name}`
    - 启动服务: `docker-compose up -d`


### 更新代码

1. 【本地执行】将本地的修改复制到部署机器中
    - 如果更新较大，可以整体打包本地代码覆盖目标机器
    - 如更新较小，可单个文件覆盖
2. 【部署机器】在项目目录执行 `docker-compose restart`

## 依赖GPU开发场景

### 寻找开发环境

> 由于gpu基本都处于远程开发环境中，因此建议寻找远程开发环境

远程开发环境有两要求:

1. 系统中存在docker
2. 安装过docker buildx

### 初始化项目

> ssh登录远程开发环境

1. `pip install algboost --upgrade`
2. `algboost-project init -t gpu <project_name>`
3. `cd <project_name>`
4. 设置git源地址: `git remote set-url origin <gitlab新建的地址>`

### 本地使用vscode远程开发

> 参考: https://code.visualstudio.com/remote/advancedcontainers/develop-remote-host

在扩展中找到 `Remote - SSH` 扩展，并安装；

1. 在远程带有gpu的机器上安装 `algboost` 并初始化工程

执行 `pip install algboost --upgrade` 安装初始化命令行工具

执行 `algboost-project init -t gpu <project_name>` 初始化工程

![step0](./lib/step0.png)

2. vscode中使用ssh远程开发扩展连接到远程开发机器

![step1](./lib/step1.png)

3. 在ssh远程开发模式下打开初始化后的目录

![step2](./lib/step2.png)

4. 打开后点击构建镜像，自动构建容器中执行环境

这一步可以修改 requirements.txt 修改自己需要安装的库，也可以等启动容器后自己在容器中安装。

如果图中弹框已经消失，可以用 `command + P` 调出命令控制，输入 `> rebuild` 选择 Devcontainer的重新构建和进入容器功能，可触发重新构建。

![step3](./lib/step3.png)

5. 经测试容器中的gpu可见

![step4](./lib/step4.png)
